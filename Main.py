import csv
import pandas as pd
import numpy as np
from networks.Perceptron import Perceptron
from networks.Adaline import Adaline

data = pd.read_csv('databases/sonar.all-data', header=None, encoding='latin1', sep=',')
# data = pd.read_csv('databases/teste', header=None, encoding='latin1', sep=',')
data.replace(['M', 'R'], [1, 0], inplace=True)
# data.replace(['Iris-setosa', 'Iris-versicolor'], [1, 0], inplace=True)

row_count = data.shape[0]
data = data.values
np.random.shuffle(data)
training_data = data[:int(row_count * 0.75), :]
test_data = data[int(row_count * 0.75):, :]

with open('databases/training.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerows(training_data)
                     
with open('databases/test.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerows(test_data)

p = Perceptron(data, training_data, test_data, 1000, {'learning_rate': 2.5 * pow(10, -3)})
p.train()
p.test()
p.train()
p.test()
p.train()
p.test()
p.train()
p.test()
p.train()
p.test()

a = Adaline(data, training_data, test_data, 1000, {'learning_rate': 2.5 * pow(10, -3), 'precision': 1 * pow(10, -6)})
a.train()
a.test()
a.train()
a.test()
a.train()
a.test()
a.train()
a.test()
a.train()
a.test()