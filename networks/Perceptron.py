from networks.BaseNetwork import BaseNetwork
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, classification_report
import matplotlib.pyplot as plt
import numpy as np


class Perceptron(BaseNetwork):
    def __init__(self, data, training_data, test_data, epochs, params):
        super().__init__(data, training_data, test_data, epochs, params)

    def neuronFunction(self, x):
        x = np.append(-1, x)
        u = np.dot(x, self.weights)
        return self.thresholdFunction(u)

    def train(self):
        self.weights = np.random.randint(0, 1 + 1, self.data.shape[1])
        initial_weights = self.weights
        e = 0
        error = True
        print(f'Starting Perceptron training with weights: {initial_weights}')
        for e in range(self.epochs):
            error = False
            # print(f'Running epoch {e + 1}.')
            # print(f'Actual weights {self.weights}.')
            for x, d in zip(self.training_inputs, self.training_outputs):
                # print(f'Checking {x} expecting {d}...')
                u = self.neuronFunction(x)
                if u != d:
                    # print(f'...got {u} instead. Changing weights...')
                    x = np.append(-1, x)
                    self.weights = self.weights + self.params['learning_rate'] * (d - u) * x
                    # print(f'new weights {self.weights}.')
                    error = True
                    break
                else:
                    # print(f'...got {u}, as expected.')
                    pass

            if not error:
                break

        if error:
            print(f'\nPerceptron training was not successful.')
        else:
            print(f'\nPerceptron training was successful.')

        print(f'\nFinal epoch: {e + 1}')
        print(f'Initial weight: {initial_weights}')
        print(f'Final weight: {self.weights}\n')

    def test(self):
        print(f'Starting Perceptron test...')

        error = False
        success_count = 0
        iterations_count = 0
        predicted_outputs = []
        for x, d in zip(self.testing_inputs, self.testing_outputs):
            # print(f'Checking {x} expecting {d}...')
            u = self.neuronFunction(x)
            predicted_outputs.append(u)
            iterations_count += 1
            if u != d:
                # print(f'...got {u} instead. Stopping test...')
                error = True
                # break
            else:
                # print(f'...got {u}, as expected.')
                success_count += 1

        if error:
            print(f'\nPerceptron test was not successful.')
        else:
            print(f'\nPerceptron test was successful.')

        cm = confusion_matrix(self.testing_outputs, predicted_outputs)
        confusion_matrix_graph = ConfusionMatrixDisplay(cm, ['Mines', 'Rocks'])
        confusion_matrix_graph.plot(cmap='Blues')
        plt.show()

        print(f'\nWeight: {self.weights}\n')
        print('Success rate: %.2f' % ((success_count / iterations_count) * 100), end='%\n')
        print(f'Classification report: {classification_report(self.testing_outputs, predicted_outputs, target_names=["Rock", "Mine"])}')
        # print(f'Confusion matrix: {cm}')
