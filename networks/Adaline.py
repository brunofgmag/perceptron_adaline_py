from networks.BaseNetwork import BaseNetwork
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, classification_report
import matplotlib.pyplot as plt
import numpy as np


class Adaline(BaseNetwork):
    def __init__(self, data, training_data, test_data, epochs, params):
        super().__init__(data, training_data, test_data, epochs, params)

    def neuronFunction(self, x):
        x = np.append(-1, x)
        return np.dot(x, self.weights)

    def train(self):
        self.weights = np.random.randint(0, 1 + 1, self.data.shape[1])
        initial_weights = self.weights
        e = 0
        p = self.training_inputs.shape[0]
        last_lms = 0
        current_lms = 0
        error = 0
        fig = plt.figure(figsize=(13, 6))
        lms_history = []
        print(f'Starting Adaline training with weights: {initial_weights}')
        for e in range(self.epochs):
            # print(f'Running epoch {e + 1}.')
            last_lms = error / p
            error = 0
            for x, d in zip(self.training_inputs, self.training_outputs):
                u = self.neuronFunction(x)
                error += np.float64(pow((d - u), 2))
                x = np.append(-1, x)
                self.weights = self.weights + self.params['learning_rate'] * (d - self.thresholdFunction(u)) * x

            current_lms = error / p
            lms_history.append(current_lms)

            # print(f'Last EQM: {last_lms}')
            # print(f'Current EQM: {current_lms}')

            if np.abs(np.subtract(current_lms, last_lms)) <= self.params['precision']:
                break

        ax = fig.add_subplot(111)
        xs = range(e + 1)
        ys = lms_history
        ax.plot(xs, ys, marker=',')
        ax.set_title(f'Learning rate = {self.params["learning_rate"]}')
        ax.set_xlabel(r'Epochs')
        ax.set_ylabel(r'LMS')
        plt.show()

        if abs(current_lms - last_lms) <= self.params['precision']:
            print(f'\nAdaline training was successful.')
        else:
            print(f'\nAdaline training was not successful.')

        print(f'\nFinal epoch: {e + 1}')
        print(f'Initial weight: {initial_weights}')
        print(f'Final weight: {self.weights}')
        print(f'Last LMS: {last_lms}')
        print(f'Current LMS: {current_lms}')
        print(f'Final precision: {np.abs(np.subtract(current_lms, last_lms))}\n')

    def test(self):
        print(f'Starting Adaline test...')

        success_count = 0
        iterations_count = 0
        predicted_outputs = []
        for x, d in zip(self.testing_inputs, self.testing_outputs):
            iterations_count += 1
            u = self.neuronFunction(x)
            predicted_outputs.append(self.thresholdFunction(u))
            if self.thresholdFunction(u) == d:
                success_count += 1

        cm = confusion_matrix(self.testing_outputs, predicted_outputs)
        confusion_matrix_graph = ConfusionMatrixDisplay(cm, ['Mines', 'Rocks'])
        confusion_matrix_graph.plot(cmap='Blues')
        plt.show()

        print(f'\nWeight: {self.weights}')
        print('Success rate: %.2f' % ((success_count / iterations_count) * 100), end='%\n')
        print(f'Classification report: {classification_report(self.testing_outputs, predicted_outputs, target_names=["Rock", "Mine"])}')
        # print(f'Confusion matrix: {cm}')
