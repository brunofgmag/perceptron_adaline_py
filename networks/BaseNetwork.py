from abc import ABC, abstractmethod
import numpy as np


class BaseNetwork(ABC):
    def __init__(self, data, training_data, test_data, epochs, params):
        self.training_inputs = training_data[:, 0:(training_data.shape[1] - 1)]
        self.training_outputs = training_data[:, -1]
        self.testing_inputs = test_data[:, 0:(test_data.shape[1] - 1)]
        self.testing_outputs = test_data[:, -1]
        self.params = params
        self.epochs = epochs
        self.data = data
        self.weights = []

    @abstractmethod
    def neuronFunction(self, inputs):
        pass

    @abstractmethod
    def train(self):
        pass

    @abstractmethod
    def test(self):
        pass

    @staticmethod
    def thresholdFunction(a):
        return 1 if a >= 0 else 0

    @staticmethod
    def signFunction(a):
        return 1 if a >= 0 else -1
